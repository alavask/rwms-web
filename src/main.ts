import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = true;
import VueMermaid from "vue-mermaid";
Vue.use(VueMermaid);

new Vue({
    render: h => h(App)
}).$mount('#app');