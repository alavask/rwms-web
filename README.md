# RWMS-Web

## Настройка окружения

Первым делом нужно подтянуть все пакеты:
```bash
npm install
```

С этим проектом удобнее всего работать через Visual Studio Code. Список обязательных к установке расширений:
1. Auto Close Tag
1. Auto Rename Tag
1. Code Spell Checker
1. Vetur
1. Vue Peek

Необходимо зайти в настройки (`Ctrl+,`), перейти в раздел *Extensions->Vetur* и установить следующие опции:
- Format > Defalut Formatter: HTML &mdash; js-beautify-html
- Format > Defalut Formatter: JS &mdash; vscode-typescript
- Format > Defalut Formatter: TS &mdash; vscode-typescript
- Format > Options: Tab Size &mdash; 4
- Format > Options: Use Tabs &mdash; True

Также в меню стоит установить автосохранение: *File -> Auto Save*.

Автоформатирование кода привязано к операции сохранения файла. Но если поставить себе автосохранение, то форматирование не будет включаться. Чтобы отформатировать код при включённом автосохранении достаточно просто несколько раз понажимать `Ctrl+S`.

## Запуск

Непосредственно командами:
```bash
npm run build
npm run serve
```

Либо через bash-скрипт:
```bash
./build.sh
```

Если проект по каким-то причинам не собирается, то первым делом нужно удалить папку *node_modules*, а затем заново сделать 
```bash
npm install
```